import sys, os, time, getopt, datetime, csv, json, urllib2

def main(params):
	pid=token=since=until=None
	try:
		options,arguments=getopt.getopt(params,"hp:t:f:u:",["page=","token=","from=","until="])
	except getopt.GetoptError:
		print manstring
		sys.exit(0x02)
	for opt, arg in options:
		if opt == '-h':
			print manstring
			sys.exit(0x00)
		elif opt in ("-p", "--page"):
			pid=arg
		elif opt in ("-t", "--token"):
			token=arg
		elif opt in ("-f", "--since"):
			since=arg
		elif opt in ("-u", "--until"):
			until=arg
	
	try:
		uri="https://graph.facebook.com/%s/posts?fields=id,from,message,created_time,name&limit=1000&access_token=%s" % (pid,token)
		if since is not None:
			uri+="&since="+since
		if until is not None:
			uri+="&until="+until
		print "\n\nDownloading..."
		data=superfetch(uri)
		print "Done.\nSaving a total of "+str(len(data))+" posts..."
		writeout(data)
		print "Done.\n\n"
	except urllib2.HTTPError as e:
		error_message = e.read()
		print error_message+"\n\nDownload failed, exiting now.\n\n"
		sys.exit(0x02)
	except KeyboardInterrupt:
		print "Cancelled by user input"
		sys.exit(0x00)

def superfetch(url):
	response=urllib2.urlopen(url)
	content=response.read()
	payload=""
	try:
		payload=json.loads(content)
	except:
		print sys.exc_info()[0].__name__
		sys.exit(0x02)
	if "data" in payload:
		out=[]
		for post in payload["data"]:
			timestamp = post["created_time"]
			msg=post['message'] if ('message' in post) else "***EMPTY***"
			name=post["name"] if ("name" in post) else "***EMPTY***"
			fbid=post["id"].split('_')
			url="https://facebook.com/"+fbid[0]+"/posts/"+fbid[1]
			out.append({
				'author': post['from']['name'].encode('ascii', 'ignore'),
				"fbid":fbid[1],
				"timestamp" : timestamp,
				'message': msg.encode('ascii', 'ignore'),
				"name" : name.encode("ascii","ignore"),
				"url":url.encode("ascii","ignore")
				})
		next=[]
		if "paging" in payload:
			next = superfetch(payload["paging"]["next"])
		return out+next
	return []

def writeout(data):
	f=open(os.getcwd()+"/fetch_"+datetime.datetime.now().strftime('%Y%m%d%H%M%S%s')+".csv","wb")
	w=csv.DictWriter(f,fieldnames=["author","fbid","timestamp","message","name","url"],delimiter=";")
	w.writerows(data)
	f.close()

manstring = "\n\nCopyright (c) 2016 Ferdinand Kuchlmayr, Universitaet Wien\n\nSyntax: superfetch.py\n\t-p <page id>\n\t-t <access token>\n\t-f <from: yyyy-mm-dd>\n\t-u <until: yyyy-mm-dd>\n\n"

if __name__ == "__main__":
	main(sys.argv[1:])